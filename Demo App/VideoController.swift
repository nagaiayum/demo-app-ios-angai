//
//  VideoController.swift
//  Demo App
//
//  Created by anagai on 2020/04/07.
//  Copyright © 2020 demoapp. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class VideoController: UIViewController {

override func viewDidLoad() {
    super.viewDidLoad()
    setVideoView()
    // Do any additional setup after loading the view.
}

func setVideoView(){

    let path = Bundle.main.path(forResource: "MESI", ofType: "mp4")
    let url = URL.init(fileURLWithPath: path!)
    let player:AVPlayer = AVPlayer.init(url: url)
    let controller:AVPlayerViewController = AVPlayerViewController.init()
    controller.player = player
    controller.view.frame = CGRect(x:0, y:0, width:self.view.bounds.size.width,height:self.view.bounds.size.height)
    self.addChild(controller)
    self.view.addSubview(controller.view)
    NotificationCenter.default.addObserver(self, selector:#selector(VideoController.didPlayToEndTime), name: .AVPlayerItemDidPlayToEndTime, object: player.currentItem)
    player.play()
}

@objc func didPlayToEndTime () {
    print("finish!!")
    self.dismiss(animated: false, completion: nil)
}
}


