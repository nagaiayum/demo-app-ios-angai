//
//  FormViewController.swift
//  Demo App
//
//  Created by anagai on 2020/04/08.
//  Copyright © 2020 demoapp. All rights reserved.
//

import UIKit

class FormViewController: UIViewController ,UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var NTextField: UITextField!
    @IBOutlet weak var TextNen: UITextField!
    
    
    var pickerView: UIPickerView = UIPickerView()
    private var list = [String]()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.showsSelectionIndicator = true
        
        let toolbar = UIToolbar(frame: CGRectMake(0, 0, 0, 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(FormViewController.done))
        let cancelItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(FormViewController.cancel))
        toolbar.setItems([cancelItem, doneItem], animated: true)
        
        self.TextNen.inputView = pickerView
        self.TextNen.inputAccessoryView = toolbar
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return list[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.TextNen.text = list[row]
    }
    
    @objc func cancel() {
        self.TextNen.text = ""
        self.TextNen.endEditing(true)
    }
    
    @objc func done() {
        self.TextNen.endEditing(true)
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func send() {
        
        self.performSegue(withIdentifier: "toSecond", sender: nil)
    }

        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! NaviFormViewController
        vc.Secondtext = NTextField.text!
    }
//    
//    func for i in 1900..<2020{
//    }
    

}
        

