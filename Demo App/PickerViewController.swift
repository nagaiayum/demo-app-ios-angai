//
//  PickerViewController.swift
//  Demo App
//
//  Created by anagai on 2020/04/03.
//  Copyright © 2020 demoapp. All rights reserved.
//

import UIKit



class PickerViewController: UIViewController {
    

    @IBOutlet weak var DipslayLabal: UILabel!
    
    @IBOutlet weak var RedBar: UISlider!
    @IBOutlet weak var GreenBar: UISlider!
    @IBOutlet weak var BlueBar: UISlider!
    
    var RedColor :Float = 0
    var GreenColor :Float = 0
    var BlueColor : Float = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func RedAction(_ sender: UISlider) {
        changecolor()
    }
    
    @IBAction func GreenAction(_ sender: UISlider) {
        changecolor()
    }
    
    @IBAction func BlueAction(_ sender: UISlider) {
        changecolor()
    }
    
    func ChangeDiplayLabelColor(){
        DipslayLabal.backgroundColor =
            UIColor(red:CGFloat(RedColor),green: CGFloat(GreenColor),blue: CGFloat(BlueColor),alpha: 1.0)
    }
    
    func changecolor(){
        RedColor = RedBar.value
        GreenColor = GreenBar.value
        BlueColor = BlueBar.value
        ChangeDiplayLabelColor()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
