//
//  MapController.swift
//  Demo App
//
//  Created by anagai on 2020/04/07.
//  Copyright © 2020 demoapp. All rights reserved.
//

//import GoogleMaps
import UIKit
import CoreLocation
import MapKit

class MapController: UIViewController, CLLocationManagerDelegate{
    @IBOutlet var mapView: MKMapView!
    var locationManager: CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()
     
        locationManager = CLLocationManager()  // 変数を初期化
        locationManager.delegate = self  // delegateとしてself(自インスタンス)を設定

        locationManager.startUpdatingLocation()  // 位置情報更新を指示
        locationManager.requestWhenInUseAuthorization()// 位置情報取得の許可を得る
        
        mapView.showsUserLocation = true
    }
        
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        let longitude = (locations.last?.coordinate.longitude.description)!
        let latitude = (locations.last?.coordinate.latitude.description)!
        print("[DBG]longitude : " + longitude)
        print("[DBG]latitude : " + latitude)
        mapView.setCenter((locations.last?.coordinate)!, animated: true)
        }
        
    
}
        
        
//Navigationbarが表示されないので、使わない
//        GMSServices.provideAPIKey("AIzaSyDF3gVaKMZaUF8IBH_GNfY_IuEWYCMpfm8")
//        
//
//            let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//            view = mapView
//
//            // Creates a marker in the center of the map.
//            let marker = GMSMarker()
//            marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//            marker.title = "Sydney"
//            marker.snippet = "Australia"
//            marker.map = mapView
//        
        
    
    

        // Do any additional setup after loading the view.

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


