//
//  WebViewController.swift
//  Demo App
//
//  Created by anagai on 2020/04/07.
//  Copyright © 2020 demoapp. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

 
    @IBOutlet weak var webView: WKWebView!
    
    
    // adjust SafeArea top space
    // portrait のみを想定
    var topPadding:CGFloat = 0
 
    override func viewDidAppear(_ animated: Bool){
        print("viewDidAppear")
 
//        let screenWidth:CGFloat = view.frame.size.width
//        let screenHeight:CGFloat = view.frame.size.height
        
        // iPhone X , X以外は0となる
        if #available(iOS 11.0, *) {
            // 'keyWindow' was deprecated in iOS 13.0: Should not be used for applications
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            topPadding = window!.safeAreaInsets.top
        }
 
//        // Webページの大きさを画面に合わせる
//        let rect = CGRect(x: 0,
//                          y: topPadding,
//                          width: screenWidth,
//                          height: screenHeight - topPadding)
//
//        let webConfiguration = WKWebViewConfiguration()
//        webView = WKWebView(frame: rect, configuration: webConfiguration)
 
        let webUrl = URL(string: "https://www.sonix.asia/")!
        let myRequest = URLRequest(url: webUrl)
        webView.load(myRequest)
 
        // インスタンスをビューに追加する
        self.view.addSubview(webView)
    }
}
